import pytest
import numpy as np

from stram.utils.export import create_styling_process_figure


@pytest.mark.parametrize("margin, background", [(8, (0, 0, 0)), (21, (199, 118, 236))])
def test_create_styling_process_figure(content_image, style_image, margin, background):
    figure = create_styling_process_figure(
        content_image, style_image, content_image, margin, background
    )

    assert figure.shape[0] > 3 * margin + content_image.shape[0]
    assert figure.shape[1] == 2 * content_image.shape[1] + 3 * margin

    for i, c in enumerate(background):
        assert np.argmax(np.bincount(figure[..., i].ravel())) == c
