from os.path import join

import pytest

from stram.utils.image_processing import load_image, load_mask


def pytest_collection_modifyitems(items):
    e2e_identifier = join('tests', 'e2e')
    unit_identifier = join('tests', 'unit')
    integration_identifier = join('tests', 'integration')

    for item in items:
        item_path = str(item.fspath)

        if unit_identifier in item_path:
            item.add_marker(pytest.mark.run(order=0))
        elif integration_identifier in item_path:
            item.add_marker(pytest.mark.run(order=1))
        elif e2e_identifier in item_path:
            item.add_marker(pytest.mark.run(order=2))


@pytest.fixture(scope='function')
def mock_summary_env(monkeypatch, tmpdir):
    monkeypatch.setenv('STRAM_SUMMARY', 'True')
    monkeypatch.setenv('STRAM_SUMMARY_PATH', str(tmpdir.mkdir('summaries')))
    monkeypatch.setenv('STRAM_SUMMARY_FREQUENCY_IMAGES', '5')


@pytest.fixture(scope='function')
def mock_non_summary_env(monkeypatch):
    monkeypatch.setenv('STRAM_SUMMARY', 'False')


@pytest.fixture(scope='session')
def content_image():
    content_image_path = join('tests', 'data', 'content_image.jpg')
    yield load_image(content_image_path)


@pytest.fixture(scope='session')
def style_image():
    style_image_path = join('tests', 'data', 'style_image.jpg')
    yield load_image(style_image_path)


@pytest.fixture(scope='session')
def content_mask():
    content_mask_path = join('tests', 'data', 'content_mask.png')
    yield load_mask(content_mask_path)


@pytest.fixture(scope='session')
def style_mask():
    style_mask_path = join('tests', 'data', 'style_mask.png')
    yield load_mask(style_mask_path)


@pytest.fixture(scope='session')
def content_image_hash():
    yield 'ee111e83c1516720b1535226f7f5c66fe27e1457d29f43ec3022839a20ab624d'


@pytest.fixture(scope='session')
def style_image_hash():
    yield '43ff32d29ec2fc9ce18d945075ed39f34af090873c69f252be8a41dead801d23'
