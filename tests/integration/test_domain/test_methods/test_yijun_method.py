import glob
from os import environ

import pytest
import numpy as np

from stram.utils.json_processing import Config
from stram.domain.methods.yijun_method import YijunMethod


@pytest.fixture(scope='module')
def config_1():
    yield Config(
        method='yijun',
        style_bottom_layer='block3_conv2',
        style_strength=0.8,
        smoothing_tool='matting_affinity',
        smoothing=dict(lambda_=1e-4, epsilon=1e-6, window_radius=1),
    )


@pytest.fixture(scope='module')
def config_2():
    yield Config(
        method='yijun',
        style_bottom_layer='block2_conv2',
        style_strength=0.5,
        smoothing_tool='guided_filter',
        smoothing=dict(epsilon=3, window_radius=5),
    )


@pytest.fixture(scope='module')
def config_3():
    yield Config(
        method='yijun',
        style_bottom_layer='block5_conv2',
        style_strength=1.0,
        smoothing_tool='off',
    )


def test_method_str():
    method = YijunMethod()
    assert str(method) == 'yijun'


def test_with_summaries(mock_summary_env, content_image, style_image, config_1):
    method = YijunMethod()
    method.set_up(config_1, content_image, style_image)
    method.process(config_1)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert not np.array_equal(synthesized_image, content_image)
    assert len(glob.glob(f'{environ["STRAM_SUMMARY_PATH"]}/*/events.out.tfevents*')) == 1


def test_no_summaries(mock_non_summary_env, content_image, style_image, config_2):
    method = YijunMethod()
    method.set_up(config_2, content_image, style_image)
    method.process(config_2)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert not np.array_equal(synthesized_image, content_image)


def test_single_masked_with_summaries(
    mock_summary_env, content_image, style_image, content_mask, config_3
):
    content_mask_copy = np.copy(content_mask)
    content_mask_copy[content_mask > 0] = 1

    method = YijunMethod()
    method.set_up(config_3, content_image, style_image, content_mask_copy)
    method.process(config_3)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert np.array_equal(
        synthesized_image[content_mask == 0], content_image[content_mask == 0]
    )
    assert not np.array_equal(
        synthesized_image[content_mask > 0], content_image[content_mask > 0]
    )
    assert len(glob.glob(f'{environ["STRAM_SUMMARY_PATH"]}/*/events.out.tfevents*')) == 1


def test_masked_no_summaries(
    mock_non_summary_env, content_image, style_image, content_mask, style_mask, config_2
):
    method = YijunMethod()
    method.set_up(config_2, content_image, style_image, content_mask, style_mask)
    method.process(config_2)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert np.array_equal(
        synthesized_image[content_mask == 0], content_image[content_mask == 0]
    )
    assert not np.array_equal(
        synthesized_image[content_mask > 0], content_image[content_mask > 0]
    )


def test_masked_shape_mismatch(
    mock_non_summary_env, config_1, content_image, style_image
):
    style_mask = np.ones((100, 100), dtype=np.uint8)
    method = YijunMethod()

    with pytest.raises(ValueError):
        method.set_up(config_1, content_image, style_image, style_mask=style_mask)


def test_masked_labels_mismatch(
    mock_non_summary_env, config_3, content_image, style_image, content_mask, style_mask
):
    content_mask_copy = np.copy(content_mask)
    content_mask_copy[content_mask == 1] = 3
    method = YijunMethod()

    with pytest.raises(ValueError):
        method.set_up(config_3, content_image, style_image, content_mask_copy, style_mask)
