import glob
from os import environ

import pytest
import numpy as np

from stram.utils.json_processing import Config
from stram.domain.methods.gatys_method import GatysMethod


@pytest.fixture(scope='module')
def config():
    yield Config(
        method='gatys',
        max_iterations=30,
        early_stopping=dict(enabled=True, delta=0.1, patience=25),
        optimizer='Adam',
        optimizer_params=dict(beta_1=0.95, epsilon=1e-6),
        learning_rate_params=dict(
            decay_steps=8, initial_learning_rate=1.0, end_learning_rate=0.5, power=2.0
        ),
        content_layers=dict(block2_conv2=1.0),
        style_layers=dict(block1_conv2=0.5, block2_conv2=0.5),
        content_loss_weight=1.0,
        style_loss_weight=0.01,
        variation_loss_weight=0.1,
    )


def test_method_str():
    method = GatysMethod()
    assert str(method) == 'gatys'


def test_with_summaries(mock_summary_env, config, content_image, style_image):
    method = GatysMethod()
    method.set_up(config, content_image, style_image)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert not np.array_equal(synthesized_image, content_image)
    assert len(glob.glob(f'{environ["STRAM_SUMMARY_PATH"]}/*/events.out.tfevents*')) == 1


def test_no_summaries(mock_non_summary_env, config, content_image, style_image):
    method = GatysMethod()
    method.set_up(config, content_image, style_image)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert not np.array_equal(synthesized_image, content_image)


def test_masked_with_summaries(
    mock_summary_env, config, content_image, style_image, content_mask, style_mask
):
    method = GatysMethod()
    method.set_up(config, content_image, style_image, content_mask, style_mask)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert np.array_equal(
        synthesized_image[content_mask == 0], content_image[content_mask == 0]
    )
    assert not np.array_equal(
        synthesized_image[content_mask > 0], content_image[content_mask > 0]
    )
    assert len(glob.glob(f'{environ["STRAM_SUMMARY_PATH"]}/*/events.out.tfevents*')) == 1


def test_masked_no_summaries(
    mock_non_summary_env, config, content_image, style_image, content_mask, style_mask
):
    method = GatysMethod()
    method.set_up(config, content_image, style_image, content_mask, style_mask)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert np.array_equal(
        synthesized_image[content_mask == 0], content_image[content_mask == 0]
    )
    assert not np.array_equal(
        synthesized_image[content_mask > 0], content_image[content_mask > 0]
    )


def test_masked_shape_mismatch(mock_non_summary_env, config, content_image, style_image):
    content_mask = np.ones((100, 100), dtype=np.uint8)
    method = GatysMethod()

    with pytest.raises(ValueError):
        method.set_up(config, content_image, style_image, content_mask)


def test_masked_labels_mismatch(
    mock_non_summary_env, config, content_image, style_image, content_mask, style_mask
):
    style_mask_copy = np.copy(style_mask)
    style_mask_copy[style_mask == 2] = 3
    method = GatysMethod()

    with pytest.raises(ValueError):
        method.set_up(config, content_image, style_image, content_mask, style_mask_copy)
