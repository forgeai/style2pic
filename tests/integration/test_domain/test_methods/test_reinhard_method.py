import glob
from os import environ

import pytest
import numpy as np

from stram.utils.json_processing import Config
from stram.domain.methods.reinhard_method import ReinhardMethod


@pytest.fixture(scope='module')
def config():
    yield Config(method='reinhard', style_strength=0.85)


def test_method_str():
    method = ReinhardMethod()
    assert str(method) == 'reinhard'


def test_with_summaries(mock_summary_env, config, content_image, style_image):
    method = ReinhardMethod()
    method.set_up(config, content_image, style_image)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert not np.array_equal(synthesized_image, content_image)
    assert len(glob.glob(f'{environ["STRAM_SUMMARY_PATH"]}/*/events.out.tfevents*')) == 1


def test_no_summaries(mock_non_summary_env, config, content_image, style_image):
    method = ReinhardMethod()
    method.set_up(config, content_image, style_image)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert not np.array_equal(synthesized_image, content_image)


def test_masked_with_summaries(
    mock_summary_env, config, content_image, style_image, content_mask, style_mask
):
    method = ReinhardMethod()
    method.set_up(config, content_image, style_image, content_mask, style_mask)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert np.array_equal(
        synthesized_image[content_mask == 0], content_image[content_mask == 0]
    )
    assert not np.array_equal(
        synthesized_image[content_mask > 0], content_image[content_mask > 0]
    )
    assert len(glob.glob(f'{environ["STRAM_SUMMARY_PATH"]}/*/events.out.tfevents*')) == 1


def test_masked_no_summaries_different_unique_label(
    mock_non_summary_env, config, content_image, style_image, content_mask, style_mask
):
    content_mask_copy = np.copy(content_mask)
    content_mask_copy[content_mask > 0] = 37
    style_mask_copy = np.copy(style_mask)
    style_mask_copy[style_mask > 0] = 115

    method = ReinhardMethod()
    method.set_up(config, content_image, style_image, content_mask_copy, style_mask_copy)
    method.process(config)

    synthesized_image = method.get_synthesized_image()
    assert synthesized_image.shape == content_image.shape
    assert synthesized_image.dtype == np.uint8
    assert np.array_equal(
        synthesized_image[content_mask == 0], content_image[content_mask == 0]
    )
    assert not np.array_equal(
        synthesized_image[content_mask > 0], content_image[content_mask > 0]
    )


def test_masked_shape_mismatch(mock_non_summary_env, config, content_image, style_image):
    content_mask = np.ones((128, 132), dtype=np.uint8)
    method = ReinhardMethod()

    with pytest.raises(ValueError):
        method.set_up(config, content_image, style_image, content_mask)


def test_masked_labels_mismatch(
    mock_non_summary_env, config, content_image, style_image, content_mask, style_mask
):
    style_mask_copy = np.copy(style_mask)
    style_mask_copy[style_mask == 2] = 5
    method = ReinhardMethod()

    with pytest.raises(ValueError):
        method.set_up(config, content_image, style_image, content_mask, style_mask_copy)
