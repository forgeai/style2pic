import sys
import argparse

sys.path.append('.')
from stram.utils.export import create_styling_process_figure
from stram.utils.image_processing import load_image, save_image


def parse_arguments():
    parser = argparse.ArgumentParser(description='Process inputs for figure export')
    parser.add_argument('-s', '--style_image', help='path to the style image')
    parser.add_argument('-c', '--content_image', help='path to the content image')
    parser.add_argument('-z', '--synthesized_image', help='path to the synthesized image')
    parser.add_argument('-o', '--output_file', help='path where to save the figure')
    parser.add_argument(
        '-m', '--margin', type=int, default=12, help='margin between images in the figure'
    )
    parser.add_argument(
        '-b',
        '--background',
        type=int,
        nargs=3,
        choices=range(0, 256),
        default=(0, 0, 0),
        help='rgb background colour in the figure',
    )
    args = parser.parse_args()
    return args


def main(args):
    style_image = load_image(args.style_image)
    content_image = load_image(args.content_image)
    synthesized_image = load_image(args.synthesized_image)

    figure = create_styling_process_figure(
        content_image, style_image, synthesized_image, args.margin, args.background
    )
    save_image(args.output_file, figure)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
