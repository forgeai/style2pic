import sys
import argparse
import logging
import logging.config
from os import makedirs, environ
from os.path import join, isfile

sys.path.append('.')
logging.config.fileConfig('logging.conf')

from stram.services.commander import get_orders
from stram.services.executor import execute_orders
from stram.utils.worklog import Worklog
from stram.utils.json_processing import json2namespace
from stram.utils.image_processing import load_image, save_image, load_mask


def parse_arguments():
    parser = argparse.ArgumentParser(description='Process configurations for stram')
    parser.add_argument('-s', '--style_image', help='path to the style image')
    parser.add_argument('-c', '--content_image', help='path to the content image')
    parser.add_argument('-f', '--config_file', help='path to the full config file')
    parser.add_argument('-o', '--output_folder', help='path to the output folder')
    parser.add_argument('-j', '--style_mask', help='path to the style mask', default=None)
    parser.add_argument(
        '-k', '--content_mask', help='path to the content mask', default=None
    )
    parser.add_argument(
        '-m', '--summaries', help='path to the summaries folder', default=None
    )
    parser.add_argument(
        '-i',
        '--image_format',
        help='file format for saving images',
        default='png',
        choices=['jpg', 'jpeg', 'png'],
    )
    args = parser.parse_args()
    return args


def main(args):
    style_image = load_image(args.style_image)
    content_image = load_image(args.content_image)
    full_config = json2namespace(args.config_file)

    style_mask = load_mask(args.style_mask) if args.style_mask is not None else None
    content_mask = load_mask(args.content_mask) if args.content_mask is not None else None

    makedirs(args.output_folder, exist_ok=True)
    worklog_path = join(args.output_folder, 'worklog.json')
    if isfile(worklog_path):
        worklog = Worklog.from_file(worklog_path)
    else:
        worklog = Worklog.new(content_image, style_image)

    if args.summaries is not None:
        environ['STRAM_SUMMARY'] = 'True'
        environ['STRAM_SUMMARY_PATH'] = args.summaries

    orders = get_orders(full_config, content_image, style_image, worklog)
    style_products = execute_orders(
        orders, content_image, style_image, content_mask, style_mask
    )

    for product in style_products:
        image_name = f'{product.styling_hash}.{args.image_format}'
        image_path = join(args.output_folder, image_name)
        save_image(image_path, product.synthesized_image)

        worklog.add_work_unit(product.styling_hash, product.styling_config)
        worklog.save_to_filepath(worklog_path)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)
