# Linux x64 with miniconda
FROM continuumio/miniconda3:latest

# Install stuff
RUN apt-get update -q -y
RUN apt-get install -y build-essential
RUN apt-get install -y libgl1-mesa-dev

# Copy app to /src
COPY . /src
WORKDIR /src

# Create env
RUN conda env create -f environment.yml
RUN export TF_XLA_FLAGS="--tf_xla_cpu_global_jit"

# expose port
EXPOSE 8080
